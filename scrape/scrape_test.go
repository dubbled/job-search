package scrape

import (
	"testing"
)

func TestGithub(t *testing.T) {
	var l []Listing
	l, err := Github()
	if err != nil {
		t.Error(err)
	}
	for _, s := range l {
		if s.Title == "" {
			t.Error("Missing listing title.")
		} else if s.URL == "" {
			t.Error("Missing listing link.")
		}
	}
}

func TestMonster(t *testing.T) {
	l, err := Monster()
	if err != nil {
		t.Error(err)
	}
	for _, s := range l {
		if s.Title == "" {
			t.Error("Missing listing title.")
		} else if s.URL == "" {
			t.Error("Missing listing link.")
		}
	}
}
