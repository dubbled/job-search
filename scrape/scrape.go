package scrape

type Listing struct {
	Title       string `xml:"title",json:"title"`
	Description string `xml:"description"`
	URL         string `xml:"link"`
	Source      string
	Type        string
	LocationRaw string `json:"location"`
	Company     string `json:"company"`
	Hash        string
	Age         string
	Tags        []string
}

type Query struct {
	Source string
}
