package scrape

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func Justice() ([]Listing, error) {
	var resp *http.Response
	var err error
	if resp, err = http.Get("https://www.justice.gov/api/v1/vacancy_announcements.json?pagesize=99"); err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	listings := make([]Listing, 1)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	r := &struct {
		Metadata struct {
			ResponseInfo struct {
				Status string `json:"status"`
				DevMSG string `json:"developerMessage"`
			} `json:"responseInfo"`
			ResultSet struct {
				Count    int `json:"count"`
				PageSize int `json:"pagesize"`
				Page     int `json:"page"`
			} `json:"resultSet"`
			Results []Listing `json:"results"`
		}
	}{}

	if err := json.Unmarshal(body, &r); err != nil {
		return nil, err
	}

	return listings, nil
}
