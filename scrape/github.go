package scrape

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func Github() ([]Listing, error) {
	var resp *http.Response
	var err error
	if resp, err = http.Get("https://jobs.github.com/positions.json"); err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	listings := make([]Listing, 1)

	var body []byte
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(body, &listings); err != nil {
		return nil, err
	}

	for _, l := range listings {
		listings = append(listings, l)
	}
	return listings[50:], nil
}
