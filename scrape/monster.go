package scrape

import (
	"encoding/xml"
	"net/http"
)

// Currently a limited amount of results. Need API key for more functionality.
func Monster() ([]Listing, error) {
	resp, err := http.Get("http://rss.jobsearch.monster.com/rssquery.ashx")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	listings := make([]Listing, 1)
	d := xml.NewDecoder(resp.Body)
	for {
		t, _ := d.Token()
		if t == nil {
			break
		}
		switch se := t.(type) {
		case xml.StartElement:
			if se.Name.Local == "item" {
				var l Listing
				if err := d.DecodeElement(&l, &se); err != nil {
					return nil, err
				}
				listings = append(listings, l)
			}
		}
	}
	return listings[1:], nil
}
