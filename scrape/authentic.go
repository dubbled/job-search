package scrape

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func Authentic() ([]Listing, error) {
	resp, err := http.Get("https://authenticjobs.com/api/?api_key=641bc28def1490b815a2a635d58c74c8&format=json&method=aj.jobs.search")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	var body []byte
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var start int
	var end int
	for i, x := range body { // Iterate through byte slice and find bonuds of JSON array.
		if string(x) == `[` {
			start = i
		} else if string(x) == `]` {
			end = i + 1
		}
	}
	var b []byte
	b = body[start:end]
	listings := make([]Listing, 1)
	err = json.Unmarshal(b, listings)
	if err != nil {
		return nil, err
	}

	return listings, err
}
